(setq doom-font (font-spec :family "Cascadia code")
      doom-variable-pitch-font (font-spec :family "Cascadia code"))

(when (display-graphic-p)
  (require 'all-the-icons))

(require 'ewal)
(ewal-load-color 'magenta -4)

(setq org-directory "~/org/")

(setq display-line-numbers-type t)

(set-frame-parameter (selected-frame) 'alpha '(85 . 50))
(add-to-list 'default-frame-alist '(alpha . (85 . 50)))

(require 'rust-mode)

(setq neo-theme (if (display-graphic-p) 'icons 'arrow))

(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

(setq user-full-name "John doe"
      user-mail-address "john@doe.com")
